const express = require('express');
const { getAllAccounts } = require('../controllers/account-controller');

const accountRouter = express.Router();

accountRouter.route('/all').get(getAllAccounts);

module.exports = accountRouter;
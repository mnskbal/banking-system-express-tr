const express = require('express');
const {getTest, postTest} = require('../controllers/test-controller');

const testRouter = express.Router();

testRouter.route('/').get(getTest).post(postTest);

module.exports = testRouter;
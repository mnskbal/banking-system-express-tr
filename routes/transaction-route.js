const express = require('express');
const transactionRouter = express.Router()

const { withdraw, deposit } = require('../controllers/transaction-controller')

transactionRouter.get('/withdraw', withdraw)
transactionRouter.get('/deposit', deposit)

module.exports = transactionRouter
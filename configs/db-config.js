const mysql = require('mysql2/promise');

const connectionConfig = {
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'banking_system'
};

const createConnection = async () => {
    const connection = await mysql.createConnection(connectionConfig);
    return connection;
};

module.exports = { createConnection };
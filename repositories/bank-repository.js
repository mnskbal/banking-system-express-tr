const asyncHandler = require('express-async-handler');
const { createConnection } = require('../configs/db-config');

const getBank = asyncHandler(async (bankId) => {
    const connection = await createConnection();
    let bank = {};
    try {
        const [rows] = await connection.query(`SELECT * FROM bank WHERE id = ${bankId}`);
        if ([rows] && [rows].length > 0) {
            bank = {
                id: rows[0].id,
                name: rows[0].name
            }
        }
        connection.end();
    } catch (error) {
        console.log(error);
    }
    return bank;
});

module.exports = { getBank };

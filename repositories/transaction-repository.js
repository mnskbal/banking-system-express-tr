const asyncHandler = require('express-async-handler');
const { createConnection } = require('../configs/db-config');

const listTransactions = asyncHandler(async (accountId) => {
    const connection = await createConnection();
    let transactions = [];
    try {
        const [rows] = await connection.query(`SELECT * FROM transaction WHERE account_id = ${accountId}`);
        if ([rows] && [rows].length > 0) {
            transactions = rows.map((row) => ({
                id: row.id,
                amount: row.amount,
                type: row.type,
                date: row.date,
                accountId: row.account_id
            }));
        }
        connection.end();
    } catch (error) {
        console.log(error);
    }
    return transactions;
});

const insertTransaction = asyncHandler(async (accountId, transaction) => {
    let status = false;
    const connection = await createConnection();
    try {
        await connection.beginTransaction();
        const [rows] = await connection.query(`INSERT INTO transaction (amount, type, date, account_id) VALUES (${transaction.amount}, "${transaction.type}", NOW(), ${transaction.accountId})`);
        console.log(`Create transaction resp: ${[rows]}`);
        await connection.commit();
        connection.end();
    } catch (error) {
        console.log(error);
        connection.rollback();
    }
    return status;
});

module.exports = { listTransactions, insertTransaction };

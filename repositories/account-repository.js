const asyncHandler = require('express-async-handler');
const { createConnection, createConnectionV2 } = require('../configs/db-config');

const listAccounts = asyncHandler(async (userId) => {
    let accounts = [];
    try {
        const connection = await createConnection();
        const [rows] = await connection.query(`SELECT * FROM account WHERE user_id = ${userId}`);
        accounts = rows.map((row) => ({
            id: row.id,
            balance: row.balance,
            bankId: row.bank_id,
            userId: row.user_id,
        }));
        connection.end();
    } catch (error) {
        console.log(error);
    }
    return accounts;
});

const fetchAccount = asyncHandler(async (accountId) => {
    let account = {};
    try {
        const connection = await createConnection();
        const [rows] = await connection.query(`SELECT * FROM account WHERE id = ${accountId}`);
        account = [rows] && [rows].length > 0 ? {
            id: rows[0].id,
            balance: rows[0].balance,
            bankId: rows[0].bank_id,
            userId: rows[0].user_id,
        } : {}
        connection.end();
    } catch (error) {
        console.log(error);
    }
    return account;
});

const updateAccount = asyncHandler(async (accountId, accountDto) => {
    let status = false;
    try {
        const connection = await createConnection();
        await connection.beginTransaction();
        const [rows] = await connection.query(`UPDATE account SET balance = ${accountDto.balance}, bank_id = ${accountDto.bankId}, user_id = ${accountDto.userId} WHERE (id = ${accountId})`);
        await connection.commit();
        connection.end();
        status = [rows] && [rows].length > 0;
    } catch (error) {
        console.log(error);
        connection.rollback();
    }
    return status;
});

const updateAccountBalance = asyncHandler(async (accountId, updatedBalance) => {
    let status = false;
    const connection = await createConnection();
    try {
        await connection.beginTransaction();
        const [rows] = await connection.query(`UPDATE account SET balance = ${updatedBalance} WHERE (id = ${accountId})`);
        await connection.commit();
        connection.end();
        status = [rows] && [rows].length > 0;
    } catch (error) {
        console.log(error);
        connection.rollback();
    }
    return status;
});

module.exports = { listAccounts, fetchAccount, updateAccount, updateAccountBalance };

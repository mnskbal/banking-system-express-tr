const { getObject } = require('../services/test-service');

const getTest = (req, res) => {
    console.log(req.query);
    let userId = req.query.userId;
    console.log(userId);
    console.log( req.headers);
    if(!userId)
        userId = req.headers.userid;
    if(!userId)
        throw new Error("A valid userId is required");
    let obj = getObject(userId);
    console.log(obj);
    res.json(obj);
}

const postTest = (req, res) => {
    console.log(req.query);
    console.log(req.body);
    let obj = getObject(req, res);
    console.log(obj);
    res.json(obj);
};

module.exports = { getTest, postTest };
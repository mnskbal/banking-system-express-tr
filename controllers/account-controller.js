const { getAccounts } = require('../services/account-service');

const getAllAccounts = async (req, res) => {
    try {
        const accounts = await getAccounts(req.query.uId);
        console.log(`URL: ${req.originalUrl} request received for all banks with uId: ${req.query.uId}.`);
        res.json(accounts);
    } catch (err) {
        throw err;
    }
};

module.exports = { getAllAccounts };
const { withdrawAmount, depositAmount } = require('../services/account-service');

const withdraw = async (req, res, next) => {
    let accountId = req.query['account-id'];
    let amount = req.query['amount'];
    console.log(accountId, amount);
    if ((!accountId || accountId <= 0) || (!amount || amount <= 0)) {
        res.status(400);
        throw new Error("A Valid account identifier and amount are required to access this end-point!");
    }
    return res.status(200).json(await withdrawAmount(accountId, amount));
};

const deposit = async (req, res, next) => {
    let accountId = req.query['account-id'];
    let amount = req.query['amount'];
    console.log(accountId, amount);
    if ((!accountId || accountId <= 0) || (!amount || amount <= 0)) {
        res.status(400);
        throw new Error("A Valid account identifier and amount are required to access this end-point!");
    }
    return res.status(200).json(await depositAmount(accountId, amount));
};

module.exports = { deposit, withdraw };

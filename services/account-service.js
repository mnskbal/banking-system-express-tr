const { listAccounts, fetchAccount, updateAccountBalance } = require('../repositories/account-repository');
const { getBank } = require('../repositories/bank-repository');
const { fetchAllTransactions, createTransaction } = require('../services/transaction-service');

const enquireBalance = (accountId) => {
    return fetchAccount(accountId).balance;
};

const withdrawAmount = async (accountId, amount) => {
    let status = false;
    let acc = await fetchAccount(accountId);
    if (acc.balance >= amount) {
        status = await updateAccountBalance(accountId, (acc.balance - amount))
            && await createTransaction(accountId, amount, 'D');
    }
    return acc;
};

const depositAmount = async (accountId, amount) => {
    let status = false;
    let acc = await fetchAccount(accountId);
    let updatedAmount = acc.balance + Number(amount);
    status = await updateAccountBalance(accountId, updatedAmount)
        && await createTransaction(accountId, amount, 'C');
    return acc;
};

const getAccount = async (accountId) => {
    let acc = await fetchAccount(accountId);
    if (acc == null)
        throw new Error(`Unable to find any account with ID# ${accountId}`);
    return acc;
};

const getAccounts = async (userId) => {
    let accounts = [];
    let accountEos = await listAccounts(userId);
    if (accountEos.length > 0) {
        const accountPromises = accountEos.map(async (a) => {
            a.bank = await getBank(a.bankId);
            a.transactions = await fetchAllTransactions(a.id);
            return a;
        });
        accounts = await Promise.all(accountPromises);
    }
    return accounts;
};

module.exports = { enquireBalance, withdrawAmount, depositAmount, getAccount, getAccounts };
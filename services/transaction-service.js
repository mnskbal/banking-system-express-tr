const { insertTransaction, listTransactions } = require('../repositories/transaction-repository');

const fetchAllTransactions = async (accountId) => {
    return await listTransactions(accountId);
};

const createTransaction = async (accountId, amount, type) => {
    return await insertTransaction(accountId, { "amount": amount, "type": type, "accountId": accountId });
};

module.exports = { fetchAllTransactions, createTransaction };

const errorHandler = (err, req, res, next) => {
    console.log(`in error handler!`);
    let status = res.statusCode ? res.statusCode : 500;
    let message = err.message ? err.message : 'some unknown error occured. Please try again later!'
    let code;
    switch (status) {
        case 400:
            code = 'Validation Failed!'
            break;
        default:
            code = 'Internal Server Error'
            break;
    }
    res.status(status).json({ "code": code, "message": message, "stackTrace": err.stack });
}

module.exports = errorHandler;
const logger = (req, res, next) => {
    console.log(`Method Entry. URL: ${req.url}`);
    next();
}

module.exports = logger;
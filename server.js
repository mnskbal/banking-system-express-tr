const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors())
app.use(require('./middlewares/log-handler'));

app.use(express.json());

app.use(require('./routes/test-route'));
app.use('/api/user/account', require('./routes/account-route'));
app.use('/api/transactions', require('./routes/transaction-route'));

app.use(require('./middlewares/error-handler'));
app.listen(8080);